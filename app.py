from flask import Flask, jsonify, request
from io import BytesIO
import base64
import re
import json
import datetime
from gtts import gTTS

import glob 
import os
import random
import IPython.display as ipd
import librosa
from PIL import Image
from flask import json
from flask import Flask, render_template,request
from flask import Flask, request, redirect, url_for
from urllib3.contrib.appengine import AppEngineManager
import certifi
from base64 import decodestring
app = Flask(__name__)
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')
@app.route('/predict', methods=['GET', 'POST'])
def upload():
    tok=request.get_json()
    token=tok['reader']
    import base64
    head,tail = token.split(';')
    _,enc = head.split(':') # TODO: check if the beginning is "charset"
    _,msg = tail.split(',') # TODO: check that the beginning is "base64"
    #image = msg.encode('utf-8')
    #encoded_image_path = text_to_image.encode(msg, "imae.png")
    with open('NewPicture.wav', 'wb') as file_to_save:
        file_to_save.write(base64.b64decode(msg))
    return ''
@app.route('/result', methods=['GET', 'POST'])
def result():
    fs = 44700 
    seconds = 6  
    #ipd.Audio('C:/Users/thiru/Downloads/vove/NewPicture.wav')
    data, sampling_rate = librosa.load('NewPicture.wav')
    n0 = 9000
    n1 = 9100
    y = data//1
    zeros = (y == 0).sum()
    ones = (y == -1).sum()
    zero_crossings = librosa.zero_crossings(data[n0:n1], pad=False)
    print(sum(zero_crossings))
    if (sum(zero_crossings) <= 10 and zeros > ones):
        print('1')
        symp= 'you seem to have sore throat and Cough'

    elif(sum(zero_crossings) <= 10 and zeros < ones):
        print('0')
        symp= 'you seem to have sore throat but no Cough'

    elif(sum(zero_crossings) > 10 and zeros > ones):
        print('0')
        symp= 'your throat seems to be fine but you have Cough'
    else:
        symp= "your throat seems to be fine and you don't have Cough"
        
    return render_template('thank.html',symp = symp)
if __name__ == "__main__":
    app.run(debug=False)